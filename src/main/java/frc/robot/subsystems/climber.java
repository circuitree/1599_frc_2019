/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Spark;

import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.Climb;
/**
 * Add your docs here.
 */
public class climber extends Subsystem {
  public Spark ups; 
  public Spark ups2;
  public Spark upD;
   public void InitClimber(){
   ups = new Spark(RobotMap.sparkClimb1);
   ups2 = new Spark(RobotMap.sparkClimb2);
   upD = new Spark(RobotMap.sparkClimbD);
   }
  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new Climb());
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
  }
}
