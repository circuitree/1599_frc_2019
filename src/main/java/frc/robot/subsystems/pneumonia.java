/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;
import frc.robot.RobotMap;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
public class pneumonia extends Subsystem {
  public DoubleSolenoid PD1;public DoubleSolenoid PD2;public DoubleSolenoid PD3;public DoubleSolenoid PD4;
  public void initPneumatics(){
  PD1 = new DoubleSolenoid(RobotMap.pneu1, RobotMap.pneu2);
  PD2 = new DoubleSolenoid(RobotMap.pneu3, RobotMap.pneu4);
  PD3 = new DoubleSolenoid(RobotMap.pneu5, RobotMap.pneu6);
  PD4 = new DoubleSolenoid(RobotMap.pneu7, RobotMap.pneu8);
  //gosh DARN this is some clean mapping
  }

  @Override
  public void initDefaultCommand() {

  }
}
