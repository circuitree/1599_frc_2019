/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.PWMTalonSRX;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.Elevate;
public class elevator extends Subsystem {
 public Spark spU; //speed up
 public PWMTalonSRX spD;//you can guess this one
 public SpeedControllerGroup ElevatorMotors;
  public void InitElevator(){
  spU = new Spark(RobotMap.sparkUp);
  spD = new PWMTalonSRX(RobotMap.sparkDown);
  ElevatorMotors = new SpeedControllerGroup( spU, spD);
  ElevatorMotors.setInverted(true);
  }
  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new Elevate());
  //i still dont know how init commands work
    
  }
}
