package frc.robot.subsystems;


import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.LidarPIDsource;
import frc.robot.Robot;
import frc.robot.RobotMap;



public class Lidar extends Subsystem {
	public LidarPIDsource PSource;
	private static Lidar instance;
	private SerialPort serial;
    public PIDController Loopster;
	volatile private int distance;
	private int distanceBuffer;
	private Thread thread;

	@Override
	protected void initDefaultCommand() {
		//setDefaultCommand(new LidarElevate());
	}

	/**
	 * Constructor. Creates a network table
	 * and a serial port object, and then creates
	 * a thread and runs it.
	 */
	public Lidar() {
		connect();
		startThread();
	}

	/**
	 * Gets the subsystem instance
	 * 
	 * @return subsystem instance
	 */
	public static Lidar getInstance() {
		return instance == null ? instance = new Lidar() : instance;
	}

	public void startThread() {
		if(thread == null) {
			thread = getThread();
			thread.start();
		}
	}

	public void stopThread() {
		if(thread != null) {
			thread.interrupt();
			thread = null;
		}
	}

	public void connect() {
		if(serial == null) {
			try {
				serial = new SerialPort(RobotMap.BAUD, SerialPort.Port.kUSB);
				System.out.println("Lidar connected on port 0");
			} catch (Exception e) {
				System.err.println("LIDAR Cannot Connect on Port 0"); //serial will be null here
				try
				{
					serial = new SerialPort(RobotMap.BAUD, SerialPort.Port.kUSB1);
					System.out.println("Lidar connected on port 1");
				}
				catch(Exception e2)
				{
					System.err.println("LIDAR Cannot Connect on Port 1"); //serial will be null here
					try
					{
						serial = new SerialPort(RobotMap.BAUD, SerialPort.Port.kUSB2);
						System.out.println("Lidar connected on port 2");
					}
					catch(Exception e3)
					{
						System.err.println("LIDAR Cannot Connect on Port 2"); //serial will be null here							
					}		
				}
			}
		}
	}

	public boolean isConnected() {
		return serial != null;
	}

	/*public void disconnect() {
		if(serial != null) {
			serial.free();
			serial = null;
		}
	}*/ // probably not useful

	/**
	 * Creates a runnable instance of the
	 * LIDAR distance reading thread. Loops and
	 * continuously reads from the Serial buffer.
	 * 
	 * @return Thread object to be run
	 */
	private Thread getThread() {
		return new Thread() {
			@Override
			public void run() {
				while (!isInterrupted()) {	
					
					if(serial != null) {
						updateDistance();
					}
					
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						return;
					}
					
				} //end while
			} //end run
		};
	}

	/**
	 * Reads current saved distance
	 * 
	 * @return distance in CM
	 */
	public int getDistance() {
		return distance;
	}

	/**
	 * Whether more than two 
	 * bytes are available to read
	 * 
	 * @return
	 */
	private boolean bytesAvailable() {
		return isConnected() && serial.getBytesReceived() > 0;
	}

	/**
	 * Reads from Serial data buffer until no data
	 * exists, and sets the value of distance
	 */
	private void updateDistance() {
		byte lastByte = 0;
		int frameIndex = -100;
		
		while(bytesAvailable()) {
			
			byte read = readByte();
			//System.out.write(read);
			if(read == 0x59 && lastByte == 0x59)
			{
				//System.out.println("Lidar begin frame");
				frameIndex = 1;
				distance = 0;
			}
			else{
				frameIndex++;
				if(frameIndex == 3)
				{
					distance = (((int)read & 0xFF) << 8) + (int)(lastByte & 0xFF);
					//System.out.print(String.format("0x%08X",read));
					//System.out.print(" ");
					System.out.println(String.format("0x%08X",lastByte));
					System.out.println((int)distance);
					
				}
			}
			
			// if (read == 13) {
			// 	//CR
			// } else if (read == 10) {
			// 	//LF, last
			// 	distance = distanceBuffer;
			// 	distanceBuffer = 0;
			// } else if (read >= 48 && read <= 57) {
			// 	//NUM
			// 	distanceBuffer *= 10;
			// 	distanceBuffer += (int)read - 48;
			// } else {
			// 	//System.err.println("xxLIDAR Read Invalid Character");
			// }
			lastByte = read;
		} //end while
	} //end method
	private byte readByte() {
		byte b = 0;
		if(bytesAvailable()) {
			return serial.read(1)[0];
		} 
		return b;
	}
	public void LidarInit2(){
		PSource = new LidarPIDsource();
		Loopster = new PIDController(.8, 0.7, 0.0, PSource, Robot.elevator.ElevatorMotors);
		Loopster.setOutputRange(-1.0,1.0 );
		
	}
	/**
	 * Updates the NetworkTable with the 
	 * most recent distance information
	 */
	public void updateSmartDashboard(	) {
		SmartDashboard.putNumber("LIDAR Distance", distance);
	}
}
