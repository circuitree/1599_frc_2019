/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;
import frc.robot.commands.*;
import edu.wpi.first.wpilibj.PWMTalonSRX;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.robot.RobotMap;  

public class driveTrain extends Subsystem {//yes its on one line. no im not going to fix it
  public PWMTalonSRX FL;public PWMTalonSRX BL;public PWMTalonSRX FR; public PWMTalonSRX BR; public SpeedControllerGroup LD; public SpeedControllerGroup RD;public DifferentialDrive driveDifference;
   public void roDr() {//init code 
    FL = new PWMTalonSRX(RobotMap.lFMotor);BL = new PWMTalonSRX(RobotMap.lBMotor);FR = new PWMTalonSRX(RobotMap.rFMotor);BR = new PWMTalonSRX(RobotMap.rBMotor);LD = new SpeedControllerGroup(FL, BL);LD.setInverted(true);RD = new SpeedControllerGroup(FR, BR);RD.setInverted(true);driveDifference = new DifferentialDrive(LD, RD);
    FL.setSafetyEnabled(false);
    FR.setSafetyEnabled(false);
    BL.setSafetyEnabled(false);//safety is for nerds and it crowds the console
    BR.setSafetyEnabled(false);}
  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new Drive());//i dont actually know how init commands work and im to afraid to ask but this seems to work
  }

}

