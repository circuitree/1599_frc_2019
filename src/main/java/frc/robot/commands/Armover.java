/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;

public class Armover extends Command {
  private int way;
  public Armover(int way) {
    requires(Robot.army);
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    this.way = way;
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    switch (way)
    {case 0 : Robot.army.vicky.set(-1.0);
      break;
      
    case 1: Robot.army.vicky.set(1.0);
  break;}
  SmartDashboard.putNumber("vicspeed", Robot.army.vicky.get());
    
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.army.vicky.setSpeed(0.0);
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
    Robot.army.vicky.setSpeed(0.0);
  }
}
