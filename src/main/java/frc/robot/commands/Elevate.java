/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
public class Elevate extends Command {
  public boolean sUp; //speed up
  public boolean sDo; // speed down
  public double netSpeed;
  public Elevate() {
  
    requires(Robot.elevator);
    
  }

  
  @Override
  protected void initialize() {

  }

 
  @Override
  protected void execute() {//deadzoned inputs are used for elevator speed, if theres phantom input we might have to dz(netSpeed) but thats unlikely
    sDo =  ( (Robot.m_oi.operatorControl.getPOV()>270 || (Robot.m_oi.operatorControl.getPOV()<90&&Robot.m_oi.operatorControl.getPOV()!=-1)));
    sUp = ( Robot.m_oi.operatorControl.getPOV()>90&&Robot.m_oi.operatorControl.getPOV()<180);
    SmartDashboard.putBoolean("lefttriggy", sDo);
    SmartDashboard.putBoolean("right-on trig", sUp);
    if (Robot.m_oi.operatorControl.getPOV()==-1){netSpeed =0;}
    else if (Robot.m_oi.operatorControl.getPOV()==0){netSpeed= 1.0;}
    else if (Robot.m_oi.operatorControl.getPOV()==180){netSpeed =-1.0;};
    SmartDashboard.putNumber("speedgo", netSpeed);// speed is a differrential between inputs so you cant make the robot sad
    Robot.elevator.ElevatorMotors.set(netSpeed);
    SmartDashboard.putNumber("POV", Robot.m_oi.operatorControl.getPOV());
  }

  
  @Override
  protected boolean isFinished() {
    return false;
  }
 private double dz(double rawSpeed){ //all the cool kids write their own deadzone code
   if (rawSpeed < .05)return 0; else return rawSpeed;
 }
 
  @Override
  protected void end() {
    dz(0);
  }

 
  @Override
  protected void interrupted() {
  }
}
