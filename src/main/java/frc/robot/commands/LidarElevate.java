/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.subsystems.Lidar;
import frc.robot.LidarPIDsource;
public class LidarElevate extends Command {
  private PIDController Loopster;
  private double netSpeed;
  private double target;
  
  public LidarElevate(double target) {  
    requires(Robot.lidar);
    requires(Robot.elevator);
    this.target = target;

    
  }

  
  @Override
  protected void initialize() {
    Robot.lidar.Loopster.enable();
    Robot.lidar.Loopster.setSetpoint(target);
  }

 
  @Override
  protected void execute() {//deadzoned inputs are used for elevator speed, if theres phantom input we might have to dz(netSpeed) but thats unlikely
    
    SmartDashboard.putNumber("lidar height", Robot.lidar.getDistance());
    SmartDashboard.putNumber("PidSpeed", Robot.elevator.spD.get());
    
  }

  
  @Override
  protected boolean isFinished() {
    int upperbound = (int) (target+2);

    int lowerbound= (int) (target - 2);
    return((upperbound)>Robot.lidar.getDistance()&& Robot.lidar.getDistance()>(lowerbound))||(Robot.m_oi.operatorControl.getPOV()!=-1);
  }
 private double dz(double rawSpeed){ //all the cool kids write their own deadzone code
   if (rawSpeed < .05)return 0; else return rawSpeed;
 }
 
  @Override
  protected void end() {Robot.lidar.Loopster.disable();
  }

 
  @Override
  protected void interrupted() {
    Robot.lidar.Loopster.disable();
  }
}
