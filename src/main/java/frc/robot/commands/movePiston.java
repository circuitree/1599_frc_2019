/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class movePiston extends Command {
  DoubleSolenoid.Value valFor = DoubleSolenoid.Value.kForward;//comparative statements need a variable on the left i think which is wack
  DoubleSolenoid.Value valBck = DoubleSolenoid.Value.kReverse;
  private boolean checkValue= false;//end condition, because this is not continuously run because toggle switches babey
  private int Pneu;
  public movePiston(int pneu) {
    requires(Robot.pneumonia);
    this.Pneu = pneu;
  }//Dear jesus please dont look at the code here
  public void move1(){
    DoubleSolenoid.Value State1 = Robot.pneumonia.PD1.get();
    if (State1 ==  valFor) {Robot.pneumonia.PD1.set(valBck);}
    else{ Robot.pneumonia.PD1.set(valFor);};}
  public void move2(){
      DoubleSolenoid.Value State2 = Robot.pneumonia.PD2.get();
      if (State2 ==  valFor) {Robot.pneumonia.PD2.set(valBck);}
      else{ Robot.pneumonia.PD2.set(valFor);};}
  public void move3(){
        DoubleSolenoid.Value State3 = Robot.pneumonia.PD3.get();
        if (State3 ==  valFor) {Robot.pneumonia.PD3.set(valBck);}
        else{ Robot.pneumonia.PD3.set(valFor);};}
  public void move4(){
          DoubleSolenoid.Value State4 = Robot.pneumonia.PD4.get();
          if (State4 ==  valFor) {Robot.pneumonia.PD4.set(valBck);}
          else{ Robot.pneumonia.PD4.set(valFor);};}
                      
      

  @Override
  protected void initialize() {
    //no initializing cause this runs a bunch and the robot gets mad otherwise
  }

  
  @Override
  protected void execute() {
    switch(Pneu){//i  made these functions to make it neat, theoretically you could put the commands as their own switch case but
      //i couldnt get it to work because of a different bug and im too lazy to change it back
      case -1 : break;
      case 0 : move1();break;
      case 1 : move2();break;
      case 2 : move3();break;
      case 3 : move4();break;
    }
  checkValue = true;
   //stops the command after it is run through any of these bad boys
  }

  @Override
  protected boolean isFinished() {
    return checkValue;
  }

  
  @Override
  protected void end() {
  }

 
  @Override
  protected void interrupted() {
  }
}
