/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;


import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;


public class Climb extends Command {
  private double upV;
  private double fupV;
  private double forV;
  public Climb() {
    requires(Robot.climber);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    upV = (Robot.m_oi.secretController.getY2());
    fupV = (Robot.m_oi.secretController.getY1()*.85);
    forV = (Robot.m_oi.driverControl.getLeftTrigger()-Robot.m_oi.driverControl.getRightTrigger() );
    SmartDashboard.putNumber("fupv", fupV);
    SmartDashboard.putNumber("upv", upV);
    SmartDashboard.putNumber("forv", forV);
    Robot.climber.ups.set(upV);
    Robot.climber.ups2.set(fupV);
    Robot.climber.upD.set(forV);
    SmartDashboard.putNumber("upvreturn", Robot.climber.ups.get());
    SmartDashboard.putNumber("fupvreturn", Robot.climber.ups2.get());
    SmartDashboard.putNumber("forvreturn", Robot.climber.upD.get());
    
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
