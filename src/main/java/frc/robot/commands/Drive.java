/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;


public class Drive extends Command {
  public double fwd = 0;//forward velocity
  public double rot = 0;//rotational motion
  public Drive() {
  requires(Robot.driveTrain);
 
  }
  @Override
  protected void initialize() {
    
  }
  @Override
  protected void execute() { 
    requires(Robot.driveTrain);
    fwd = Robot.m_oi.driverControl.getY1();//set motor output to left controller lY and rX
    rot = Robot.m_oi.driverControl.getY2();//i think this one is negative idk we will see wont we
    Robot.driveTrain.driveDifference.tankDrive(-rot,-fwd, true );
  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
  }

  @Override
  protected void interrupted() {
  }
}
