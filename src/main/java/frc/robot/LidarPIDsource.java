package frc.robot;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
public class LidarPIDsource implements PIDSource{
		public double pidGet(){return  Robot.lidar.getDistance();
    }
        public void setPIDSourceType(PIDSourceType pidsource){ }

		@Override
		public PIDSourceType getPIDSourceType() {
			return PIDSourceType.kDisplacement;
		}
		
		
	}