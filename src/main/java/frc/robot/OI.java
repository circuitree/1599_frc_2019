/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.XboxController;
import frc.robot.commands.*;
import frc.robot.subsystems.myLimeLight;
/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the comm`ands and command groups that allow control of the robot.
 */
public class OI {public XboxController driverControl;
  public Joystick operatorControl;
  private JoystickButton X;
  private JoystickButton A;
  private JoystickButton B;
  private JoystickButton Y;
  public JoystickButton RB;
  private JoystickButton LB;
  private JoystickButton Share;
  private JoystickButton Options;
  private JoystickButton L3;
  private JoystickButton R3;
  public XboxController secretController;
  //// CREATING BUTTONS 
  // One type of button is a joystick button which is any button on a
  //// joystick.
  // You create one by telling it which joystick it's on and which button
  // number it is.
  // Joystick stick = new Joystick(port);
  // Button button = new JoystickButton(stick, buttonNumber);
public void oi() { 
  secretController = new XboxController(RobotMap.joyLeft);
  driverControl= new XboxController(0);
  operatorControl= new Joystick(RobotMap.joyRight);
  RB = new JoystickButton(operatorControl, 6);
  LB = new JoystickButton(operatorControl, 5);
  X = new JoystickButton(operatorControl, 3);
  Y = new JoystickButton(operatorControl, 4);
  B = new JoystickButton(operatorControl, 2);
  Share = new JoystickButton(operatorControl,7 );
  Options = new JoystickButton(operatorControl,8 );
  L3= new JoystickButton(operatorControl,9 );
  R3= new JoystickButton(operatorControl,10 );
    // SmartDashboard Buttons
  SmartDashboard.putData("MyLimeLight_get_Data", new MyLimeLight_get_Data());
  SmartDashboard.putData("myLimeLight_set_Pipeline(0)", new myLimeLight_set_Pipeline(0));
  SmartDashboard.putData("myLimeLight_set_Pipeline(1)", new myLimeLight_set_Pipeline(1));
  SmartDashboard.putData("myLimeLight_Toggle_CamMode", new myLimeLight_Toggle_CamMode());
  SmartDashboard.putData("myLimeLight_Toggle_LED", new myLimeLight_Toggle_LED());
  SmartDashboard.putData("myLimeLight_Toggle_Snapshot", new myLimeLight_Toggle_Snapshot());
  SmartDashboard.putData("myLimeLight_Toggle_Stream", new myLimeLight_Toggle_Stream());

 

  //// TRIGGERING COMMANDS WITH BUTTONS
  // Once you have a button, it's trivial to bind it to a button in one of
  // three ways:

  // Start the command when the button is pressed and let it run the command
  // until it is finished as determined by it's isFinished method.
  // button.whenPressed(new ExampleCommand());
  //add a height for flippage of the thingy and maybe one for teleops
driverControl.a.whenPressed(new movePiston(0));
Share.whenPressed(new movePiston(1));
driverControl.x.whenPressed(new movePiston(2));
Options.whenPressed(new movePiston(3));
secretController.rightJoyButton.whileHeld(new justGoDown(1));
secretController.leftJoyButton.whileHeld(new justGoDown(2));
driverControl.leftBumper.whileHeld(new Armover(0));
driverControl.rightBumper.whileHeld(new Armover(1));
B.whenPressed(new LidarElevate(RobotMap.shipHeight));
LB.whenPressed(new LidarElevate(RobotMap.hatchHeights[0]));
X.whenPressed(new LidarElevate(RobotMap.hatchHeights[1]));
Y.whenPressed(new LidarElevate(RobotMap.hatchHeights[2]));
R3.whenPressed(new LidarElevate(RobotMap.ballHeights[0]));
L3.whenPressed(new LidarElevate(RobotMap.ballHeights[1]));
RB.whenPressed(new LidarElevate(99));  
  // Run the command while the button is being held down and interrupt it once
  // the button is released.
  // button.whileHeld(new ExampleCommand());

  // Start the command when the button is released and let it run the command
  // until it is finished as determined by it's isFinished method.
  // button.whenReleased(new ExampleCommand());
}}
