/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.SerialPort;

/**
 * This is where the ports live, get you one!
 */
public class RobotMap {
  public static int lBMotor = 0;//these are PWM ports
  public static int rBMotor = 2;
  public static int lFMotor = 1;
  public static int rFMotor = 3;
  public static int sparkUp = 5;
  public static int sparkDown = 4; 
  public static int sparkClimb1 = 6;
  public static int sparkClimb2 = 7;
  public static int sparkClimbD = 8;  
  public static int redLine = 9; //?  
  public static int joyLeft = 1;//these are NOT usb ports, they are the order the dashboard detects controllers in USB 
  public static int joyRight = 2;//(order plugged in, then port priority)
  public static int pneu1 = 0;//PCM ports, good mcfreakin luck to electronics for managing to use 8 ports for 10 functions
  public static int pneu2 = 1;
  public static int pneu3 = 2;
  public static int pneu4 = 3;
  public static int pneu5 = 4;
  public static int pneu6 = 5;
  public static int pneu7 = 6;
  public static int pneu8 = 7;

  
  //heights are in cm, also not part of the robot, but you know.
  //height array values should probably be in accending order
  public static final double[] hatchHeights = {38.0, 109.0, 180.0}; // the height to move each elevator to for hatch, in cm
  public static final double[] ballHeights = {72.0, 143.0}; //in cm, for top use 
  public static final double shipHeight = 103.0;


  public final static SerialPort.Port PORT = SerialPort.Port.kUSB2;
		
		//VALUES:
		public final static int BAUD = 115200;
	

  public static int POV = 0;

}

